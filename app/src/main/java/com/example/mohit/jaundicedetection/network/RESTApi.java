package com.example.mohit.jaundicedetection.network;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by kashif on 14/3/18.
 */

public interface RESTApi {
    @POST("/histogram")
    Observable<ResponseModel> getDetection(@Body String encodedImage);
}
