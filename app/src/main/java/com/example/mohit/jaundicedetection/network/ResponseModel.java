package com.example.mohit.jaundicedetection.network;

/**
 * Created by kashif on 14/3/18.
 */

public class ResponseModel {
    private boolean error;
    private String errorMessage;
    private boolean hasJaundice;

    public ResponseModel() {}

    public ResponseModel(boolean error, String errorMessage, boolean hasJaundice) {
        this.error = error;
        this.errorMessage = errorMessage;
        this.hasJaundice = hasJaundice;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isHasJaundice() {
        return hasJaundice;
    }

    public void setHasJaundice(boolean hasJaundice) {
        this.hasJaundice = hasJaundice;
    }
}
