package com.example.mohit.jaundicedetection.network;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kashif on 14/3/18.
 */

public class ApiClient {
    private static Retrofit instance;

    private static Retrofit getRetrofitClient() {
        if(instance == null) {
            instance = new Retrofit.Builder()
                    .baseUrl("http://192.168.43.173:3000/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return instance;
    }

    public static RESTApi getRestClient() {
        return getRetrofitClient().create(RESTApi.class);
    }


}
