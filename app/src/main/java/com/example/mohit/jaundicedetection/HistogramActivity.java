package com.example.mohit.jaundicedetection;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mohit.jaundicedetection.network.ApiClient;
import com.example.mohit.jaundicedetection.network.RESTApi;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HistogramActivity extends AppCompatActivity {
    Button buttonSelectImage;
    private int PICK_IMAGE_REQUEST=1;
    private ImageView imageView;

    RESTApi restApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histogram);

        restApi = ApiClient.getRestClient();

        buttonSelectImage=(Button)findViewById(R.id.buttonSelectImage);
        imageView = (ImageView) findViewById(R.id.image_view);
        buttonSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getIntent=new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");
                Intent pickIntent=new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                //Intent chooserIntent=new Intent.createChooser(pickIntent,"Select Image");
                startActivityForResult(Intent.createChooser(getIntent,"select image"),PICK_IMAGE_REQUEST);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data)
    {
        if(requestCode==PICK_IMAGE_REQUEST && resultCode == RESULT_OK){
            Log.d("HistogramActivity", "Image picked");
            try {
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imageView.setImageBitmap(bitmap);
                processImage(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void processImage(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        restApi.getDetection(encoded)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((response) -> {
                    if(response.getError()) {
                        Toast.makeText(HistogramActivity.this, response.getErrorMessage(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(HistogramActivity.this, "Has Jaundice: " + response.isHasJaundice(), Toast.LENGTH_LONG).show();
                    }
                },
                        (error) -> {
                            Toast.makeText(HistogramActivity.this, "Network error: " + error.toString(), Toast.LENGTH_LONG).show();
                        }
                );

    }
}
